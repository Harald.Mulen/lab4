package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;
    private int cols;
    private CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows=rows;
        this.cols=columns;
        grid= new CellState[rows][cols];
        for (int row =0; row<rows;row++){
            for (int col =0; col<cols; col++){
                this.set(row, col, initialState);
            }
        }

	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        grid[row][column] = element;
        
    }

    @Override
    public CellState get(int row, int column) {
        CellState retVal = grid[row][column];
        return retVal;
    }

    @Override
    public IGrid copy() {
        CellGrid retGrid = new CellGrid(this.rows,this.cols, CellState.DEAD);
        for (int row =0; row<rows;row++){
            for (int col =0;col<cols;col++){
            retGrid.set(row, col, this.get(row, col));
            }
        }
        return retGrid;
    }
    
}
