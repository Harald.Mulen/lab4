package cellular;
import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton{

    IGrid currentGeneration;

    //Constuctor method
    public BriansBrain(int rows, int cols){
        currentGeneration= new CellGrid(rows,cols,CellState.DEAD);
    }

    @Override
    public CellState getCellState(int row, int column) {
        return currentGeneration.get(row, column);
    }

    public void setCellState(int row, int column, CellState state){
        this.currentGeneration.set(row, column, state);
    }

    @Override
    public void initializeCells() {
        Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
                int randInt=random.nextInt(3);
				if (randInt==0) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else if(randInt==1){
					currentGeneration.set(row, col, CellState.DEAD);
				}else{
                    currentGeneration.set(row,col,CellState.DYING);
                }
			}
		} 
    }

    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();
        for (int row=0; row<this.numberOfRows();row++){
            for (int col=0; col<this.numberOfColumns();col++){
                nextGeneration.set(row, col, getNextCell(row, col));
            }
        }
        currentGeneration=nextGeneration;
        
    }

    @Override
    public CellState getNextCell(int row, int col) {
        CellState state = getCellState(row, col);
        if (state==CellState.ALIVE){
            return CellState.DYING;
        }else if (state==CellState.DYING){
            return CellState.DEAD;
        }else if (state==CellState.DEAD && countNeighbors(row, col, CellState.ALIVE)==2){
            return CellState.ALIVE;
        }else{
            return CellState.DEAD;
        }
    }

    @Override
    public int numberOfRows() {
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        return currentGeneration.numColumns();
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }

    public int countNeighbors(int row, int col, CellState state){
        int count =0;
		for(int i=row-1;i<=row+1;i++){
			for (int j=col-1;j<=col+1;j++){
				try{
					if (getCellState(i, j)==state){
						count+=1;
					}
				} catch(Exception e){
				}
			}
		}
		if(this.getCellState(row, col)==state){
			count-=1;
		}
		return count;
    }
}